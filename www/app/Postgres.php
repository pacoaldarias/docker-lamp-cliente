<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once 'InDatos.php';
include_once 'cliente.php';

class Postgres implements InDatos {

    //private $pdo;
    protected $pdo;

    public function __construct() {
        $this->pdo = NULL;

        $bdhostname = "ec2-52-6-143-153.compute-1.amazonaws.com";
        $bdnombre = "d5d8lr0at0lio4";
        $bdusuario = "ziquwnrplaizrx";
        $bdpass = "660186c7c8df20f32f696edf97dbe89be536f7cf000e2e2d4394e0aedce6105e";
        $dbport = 5432;
        $dbsn = "pgsql:host=$bdhostname;port=$dbport;dbname=$bdnombre;user=$bdusuario;password=$bdpass";

        try {
            $dbconexion = new PDO($dbsn);
            $dbconexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            //$pdo = $dbconexion;
            $this->pdo = $dbconexion;
        } catch (PDOException $ex) {
            echo "ERROR: Ha Ocurrido un error con al base de datos." . $ex->getMessage();
        }
    }

    public function userPassword($usuario, $password) {
        try {
            $registro = $this->pdo->prepare("SELECT usuario, password FROM cliente WHERE usuario = :usuario AND password = :password");

            $registro->bindValue(':usuario', $usuario);
            $registro->bindValue(':password', $password);
            $registro->execute();

            $numero_registro = $registro->rowCount();

            $pdo = false;
            return $registro->fetchAll(PDO::FETCH_OBJ);

            //return $resultSet->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    //------------------------------------------------------------------------------ 
//Listamos todos los clientes
    public function ListarCl() {
        try {
            $resultSet = $this->pdo->prepare("SELECT * FROM cliente");
            $resultSet->execute();
            
            $pdo = false;
            return $resultSet->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------ 
//Obtenemos los clientes por id
    public function ObtenerCl($id) {
        try {
            $resultSet = $this->pdo->prepare("SELECT * FROM cliente WHERE id= ?");
            $resultSet->execute(array($id));
            
            $pdo = false;
            return $resultSet->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------ 
    public function ObtenerNombre($nombre) {
        try {

            $resultSet = $this->pdo->prepare("SELECT * FROM cliente WHERE Nombre= ?");
            $resultSet->execute(array($nombre));
            
            $pdo = false;
            return $resultSet->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------    
//Funcion para Buscar los clientes por su nombre
    public function Buscar_Clientes($nombre) {
        try {
            $nombre = htmlspecialchars($nombre);
            $resultSet = $this->pdo->query("SELECT * FROM cliente WHERE Nombre LIKE  '" . $nombre .
                    "' ORDER BY dni DESC");

            while ($data = $resultSet->fetchAll(PDO::FETCH_OBJ)) {
                print_r($data);
                $clientes[] = $data[0];
            }
            
            
            return $clientes;
            $pdo = false;
        } catch (Exception $ex) {
            die($ex->getMessage());
            $ex->getLine();
        }
    }

//------------------------------------------------------------------------------ 
//Funcion para Eliminar los clientes por su Id
    public function EliminarCl($id) {
        try {
            $resultSet = $this->pdo->prepare("DELETE FROM cliente WHERE id= ?");
            $resultSet->execute(array($id));
            
            $pdo = false;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------ 
//Funcion para Actualizar loc clientes
    public function ActualizarCl($data) { //Probando ......
        //print_r($data);
        try {
            $consulta = "UPDATE cliente SET 
                        dni        = ?,
			Nombre     = ?, 
			Apellido   = ?,
                        Correo     = ?,
                        Telefono   = ?,
                        usuario    = ?,
                        password   = ?
		WHERE id= ?";

            //echo $consulta;

            $this->pdo->prepare($consulta)
                    ->execute(
                            array(
                                $data->dni,
                                $data->Nombre,
                                $data->Apellido,
                                $data->Correo,
                                $data->Telefono,
                                $data->usuario,
                                $data->password,
                                $data->id
                            )
            );
            
            $pdo = false;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------ 
//Funcion para registro de nuevo cliente
    public function RegistrarCl($data) {
        try {
            $consulta = "INSERT INTO cliente (dni,Nombre,Apellido,Correo,Telefono,usuario,password) 
		        VALUES (?, ?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($consulta)
                    ->execute(
                            array(
                                //$data->id,
                                $data->dni,
                                $data->Nombre,
                                $data->Apellido,
                                $data->Correo,
                                $data->Telefono,
                                $data->usuario,
                                $data->password,
                            )
            );
            
            $pdo = false;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------ 
//Funcion para obtener cliente ordenado por su id
    public function obtener() {
        try {
            $resultSet = $this->pdo->prepare("SELECT * FROM cliente ORDER BY id");
            $resultSet->execute();

            while ($data = $resultSet->fetchAll(PDO::FETCH_OBJ)) {
                $cliente[] = $data;
            }
            
            $pdo = false;
            return $cliente;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------ 
//Funcion para crear fichero Json 
    public function jsonCl() {
        try {

            $resultSet = $this->pdo->prepare("SELECT * FROM cliente");
            $resultSet->execute();

            while ($data = $resultSet->fetchAll(PDO::FETCH_OBJ)) {
                $cliente[] = $data;
            }

            //Creamos json
            //$cliente['cliente'] = $cliente;
            $json_string = json_encode($cliente);
            echo "<br>";
            echo $json_string;

            //crear archivo json
            $file = '../ficheros/clientes.json';
            file_put_contents($file, $json_string);
            echo "<br>";
            
            $pdo = false;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------ 
//Funcion para crear fichero csv
    public function csvCl() {
        try {
            $resultSet = $this->pdo->prepare("SELECT * FROM cliente");
            $resultSet->execute();

            $datos = $resultSet->fetchAll(PDO::FETCH_OBJ);

            $file = '../ficheros/clientes.csv';
            $linea = 0;

            $archivo = fopen($file, 'r');

            while (($datos = fgetcsv($archivo, ",")) == true) {
                $num = count($datos);
                $linea++;

                for ($columna = 0; $columna < $num; $columna++) {
                    echo $datos[$columna] . "<br>";
                }
            }
            fclose($archivo);
            $pdo = false;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

//------------------------------------------------------------------------------ 
//Funcion para crear obtener RSS
    public function rssCl() {
        try {
            $resultSet = $this->pdo->prepare("SELECT * FROM cliente");
            $resultSet->execute();

            while ($data = $resultSet->fetchAll(PDO::FETCH_OBJ)) {
                $cliente[] = $data;
            }
            $cliente['cliente'] = $cliente;
            
            $pdo = false;
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    public function getPdo() {
        $pdo = false;
        return $this->pdo;
    }

}

?>