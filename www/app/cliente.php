<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class cliente {

    public $id;
    public $dni;
    public $Nombre;
    public $Apellido;
    public $Correo;
    public $Telefono;
    public $usuario;
    public $password;
    
    //------------------------------------------------------------------------------    
    //METODOS GETTERS 
    public function getId() {
        return $this->id;
    }

    public function getDni() {
        return $this->dni;
    }

    public function getNombre() {
        return $this->Nombre;
    }

    public function getApellido() {
        return $this->Apellido;
    }

    public function getCorreo() {
        return $this->Correo;
    }

    public function getTelefono() {
        return $this->Telefono;
    }

    public function getUsuario() {
        return $this->usuario;
    }

    public function getPassword() {
        return $this->password;
    }
//------------------------------------------------------------------------------
    public function setId($id){
        $this->id = $id;
    }
    public function setDni($dni){
        $this->dni = $dni;
    }
    public function setNombre($Nombre){
        $this->Nombre = $Nombre; 
    }
    public function setApelliddo($Apellido){
        $this->Apellido = $Apellido; 
    }
    public function setCorreo($Correo){
        $this->Correo = $Correo; 
    }
    public function setTelefono($Telefono){
        $this->Telefono = $Telefono; 
    }
    public function setUsuario($usuario){
        $this->usuario = $usuario; 
    }
    public function setPassword($password){
        $this->password = $password; 
    }
}

?>
