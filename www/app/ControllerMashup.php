<?php

//app/ControllercMashup.php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class ControllerMashup {

    public function mashupTNT() {

        if (isset($_POST['texto'])) {
            $texto = $_POST['texto'];
        } else {
            $texto = '';
        }
        if (isset($_POST['fecha_inicio'])) {
            $fecha_inicio = str_replace('-', '', $_POST['fecha_inicio']);
        } else {
            $fecha_inicio = '';
        }
        if (isset($_POST['fecha_fin'])) {
            $fecha_fin = str_replace('-', '', $_POST['fecha_fin']);
        } else {
            $fecha_fin = '';
        }
        $url="https://api.nytimes.com/svc/search/v2/articlesearch.json?q=".$texto."&begin_date=".$fecha_inicio."&end_date=".$fecha_fin."&api-key=QxYsdLpxP3Pw1fKNQExvZ3hbd4q0pZGF";

        //$url = "https://api.nytimes.com/svc/search/v2/articlesearch.json?fq=" . $texto . "&begin_date=" . $fecha_inicio . "&end_date=" . $fecha_fin . "&api-key=T8cvGgEzodB96Cv2WZIuiWlTLb8DOraq";
         $result = json_decode(file_get_contents($url));
         //$result = json_decode($url);
        require_once __DIR__ . '/templates/mashupTheNewYorkTimes.php';
    }
    
    public function mashupAemet() {
        require_once __DIR__ . '/templates/mashupAEMET.php';
    }
    
    

    
}

?>
