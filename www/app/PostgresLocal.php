<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once 'InDatos.php';

class PostgresLocal implements InDatos {

    public static function conexionDB() {

        $bdhostname = "localhost";
        $bdnombre = "ceedcv";
        $bdusuario = "alumno";
        $bdpass = "alumno";
        $dbport = 5432;
        $dbsn = "pgsql:host=$bdhostname;port=$dbport;dbname=$bdnombre;user=$bdusuario;password=$bdpass";

        try {
            $dbconexion = new PDO($dbsn);
            $dbconexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
            echo "ERROR: Ha Ocurrido un error con al base de datos." . $ex->getMessage();
        }
        return $dbconexion;
    }

}
