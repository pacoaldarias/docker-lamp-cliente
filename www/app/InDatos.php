<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

interface InDatos {

    public function userPassword($usuario, $password);

    public function getPdo();

    public function __construct();

    public function ListarCl();

    public function ObtenerCl($id);

    public function ObtenerNombre($nombre);

    public function Buscar_Clientes($nombre);

    public function EliminarCl($id);

    public function ActualizarCl($data);

   //public function RegistrarCl(cliente $data);
    public function RegistrarCl($data);
    
    public function obtener();

    public function jsonCl();

    public function csvCl();

    public function rssCl();
}

?>