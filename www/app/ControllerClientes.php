<?php

//app/ControllercClientes.php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once '../app/cliente.php';
include_once '../app/Mysql.php';
include_once '../app/Postgres.php';

class ControllerClientes {

    //private $model;
    private $db;

    public function __construct() {

        //$datos = $_SESSION['datos'];

        switch ($_SESSION['datos']) {
            case 'Mysql':
                $db = new Mysql();
                //$this->db = new Mysql();
                break;
            case 'Postgres':
                $db = new Postgres();
                //$this->db = new Postgres();
                break;
        }
        $_SESSION['datos'] = $_POST['datos'];
    }

//Cerre Sesion
    public function cerrarSesion() {
        try {
            require_once __DIR__ . '/templates/logout.php';
        } catch (Exception $ex) {
            echo 'ERROR: Al cerrar la sesión' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

    /* Hacemos crud de cliente tanto para cuando editemos un cliente, como agreguemos */

    public function CrudCliente() {
        try {
            $cliente = new cliente();
            //$datos = $_SESSION['datos'];
            switch ($_SESSION['datos']) {
                case 'Mysql':
                    $db = new Mysql();
                    break;
                case 'Postgres':
                    $db = new Postgres();
                    break;
            }
            $_SESSION['datos'] = $_POST['datos'];
            
            if (isset($_REQUEST['id'])) {
                $cliente = $this->db->ObtenerCl($_REQUEST['id']);
               
            }
            //$_SESSION['datos'] = $_POST['datos'];
            require_once __DIR__ . '/templates/header.php';
            require_once __DIR__ . '/templates/clienteActualizar.php';
        } catch (Exception $ex) {
            echo 'ERROR: Al hacer el Crud el cliente' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

    /* Guardar clientes */

    public function GuardarCliente() {
        try {
            $cliente = new cliente();

            //$datos = $_SESSION['datos'];
            switch ($_SESSION['datos']) {
                case 'Mysql':
                    $db = new Mysql();
                    break;
                case 'Postgres':
                    $db = new Postgres();
                    break;
            }
             $_SESSION['datos'] = $_POST['datos'];
            $cliente->id = $_REQUEST['id'];
            $cliente->dni = $_REQUEST['dni'];
            $cliente->Nombre = $_REQUEST['Nombre'];
            $cliente->Apellido = $_REQUEST['Apellido'];
            $cliente->Correo = $_REQUEST['Correo'];
            $cliente->Telefono = $_REQUEST['Telefono'];
            $cliente->usuario = $_REQUEST['usuario'];
            $cliente->password = $_REQUEST['password'];

            $cliente->id > 0 
                    ? $this->db->ActualizarCl($cliente) 
                    : $this->db->RegistrarCl($cliente);

            header('Location: index.php?ctl=clientes');
        } catch (Exception $ex) {
            echo 'ERROR: Al guardar el cliente' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

/*Esta función gestiona la visualización de los clientes y los muestra */
    public function verCliente() {
        try {
            
            $id = $_GET["id"];
            //$datos = $_SESSION['datos'];
            switch ($_SESSION['datos']) {
                case 'Mysql':
                    $db = new Mysql();
                    break;
                case 'Postgres':
                    $db = new Postgres();
                    break;
            }
             $_SESSION['datos'] = $_POST['datos'];
            //$cliente = new cliente();
            $cliente = $this->db->ObtenerCl($id);
           //$cliente = $db->ObtenerCl($id);

            require_once '../app/templates/header.php';
            require_once __DIR__ . '/templates/clienteVer.php';
        } catch (Exception $ex) {
            echo ' ERROR: Al ver los clientes' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

    /* Eliminar Cliente */

    public function borrarCliente() {
        try {
            //$datos = $_SESSION['datos'];
            switch ($_SESSION['datos']) {
                case 'Mysql':
                    $db = new Mysql();
                    break;
                case 'Postgres':
                    $db = new Postgres();
                    break;
            }
             $_SESSION['datos'] = $_POST['datos'];
            $cliente = $_GET['id'];
            $cliente = $this->db->EliminarCl($_POST['id']);

            header('Location: index.php?ctl=clientes');
        } catch (Exception $ex) {
            echo 'ERROR: Al eliminar el cliente' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

    /* Busca el artículo con comodín, Busca el cliente con comodín */

    public function buscarCliente() {
        try {
            //$datos = $_SESSION['datos'];
            switch ($_SESSION['datos']) {
                case 'Mysql':
                    $db = new Mysql();
                    break;
                case 'Postgres':
                    $db = new Postgres();
                    break;
            }
             $_SESSION['datos'] = $_POST['datos'];
            $param = array(
                'Nombre' => '',
                'resultado' => array(),
            );
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $param['Nombre'] = $_POST['Nombre'];
                $param['resultado'] = $this->db->Buscar_Clientes($_POST['Nombre']);
            }
            require_once '../app/templates/header.php';
            require_once __DIR__ . '../templates/clienteBuscar.php';
        } catch (Exception $ex) {
            echo 'ERROR: Al buscar el cliente' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

//------------------------------------------------------------------------------    
    /* Crea el JSON y lo muestra */
    public function jsonCliente() {
        try {
            //$datos = $_SESSION['datos'];
            switch ($_SESSION['datos']) {
                case 'Mysql':
                    $db = new Mysql();
                    break;
                case 'Postgres':
                    $db = new Postgres();
                    break;
            }
             $_SESSION['datos'] = $_POST['datos'];
            $result = $this->db->jsonCl();
            header("Location: ../app/templates/Cliente_json.php");
        } catch (Exception $ex) {
            echo 'ERROR: Al crear y mostrar el fichero JSON' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

    /* Crea el RSS y lo muestra */

    public function rssCliente() {
        try {
           // $datos = $_SESSION['datos'];
            switch ($_SESSION['datos']) {
                case 'Mysql':
                    $db = new Mysql();
                    break;
                case 'Postgres':
                    $db = new Postgres();
                    break;
            }
            $result = $this->db->rssCl();
            require __DIR__ . '../templates/Cliente_rss.php';
        } catch (Exception $ex) {
            echo 'ERROR: Al crear y mostrar el fichero RSS' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

    /* Crea el CSV y lo muestra */

    public function csvCliente() {
        try {
            //$datos = $_SESSION['datos'];
            /*switch ($_SESSION['datos']) {
                case 'Mysql':
                    $db = new Mysql();
                    break;
                case 'Postgres':
                    $db = new Postgres();
                    break;
            }*/
            $result = $this->db->csvCl();
            require_once ('../app/templates/Cliente_csv.php');
        } catch (Exception $ex) {
            echo 'ERROR: AL crear y mostrar el fichero CSV' . $ex->getMessage();
            echo $ex->getLine();
        }
    }

}

?>
