<?php
    session_start();
    ob_start();
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    if ($_SESSION['SessionValida'] != 1) {
        header("Location:../web/index.php");
    }

    if (isset($_REQUEST["estaciones"])) {

    // API Key requerida para acceso a APIREST OpenData
        $apikey = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmcmFua3JtMThAZ21haWwuY29tIiwianRpIjoiOWRiN2JjOWMtN2YwNS00YTljLTlkYzItNWZmZTNhNTEwZDMzIiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE1ODkyNzczMDEsInVzZXJJZCI6IjlkYjdiYzljLTdmMDUtNGE5Yy05ZGMyLTVmZmUzYTUxMGQzMyIsInJvbGUiOiIifQ.GkOhhyGzAs19idmsuyngURXjU_Zky9lB_FH6Zdcbb4g";
        $codEstacion = $_REQUEST["estaciones"]; // recogemos valor de select con el id de la estación metereológica

        /**
         * Función que devuleve la url de donde proceden los datos de AEMT
         *
         * @param [int] $codEstacion
         * @param [string] $apikey
         * @return [string] $respuesta Devuelve la URL de AEMET
         */
        function API($codEstacion, $apikey) {
            $url = "https://opendata.aemet.es/opendata/api/observacion/convencional/datos/estacion/";
            $respuesta = $url . $codEstacion . "/?api_key=" . $apikey;
            return $respuesta;
        }

        $fuente = API($codEstacion, $apikey); // llamada a función
        $json = file_get_contents($fuente); // guardamos datos en formato json
        $datos = json_decode($json, true); // decodificamos datos y transformamos json a array
        error_reporting(0);

        $fuenteDatos = $datos["datos"]; // obtenemos url con datos de la estación metereológica
        $metaDatos = $datos["metadatos"]; // obtenemos los metadatos de los campos de la información
    // convertimos la información a formato json
        $json_fuentedatos = file_get_contents($fuenteDatos);
        $json_metadatos = file_get_contents($metadatos);
    // decodificamos información pasando json a array
        $datos_fuenteDatos = json_decode($json_fuentedatos, true);
        $datos_metadatos = json_decode($json_metadatos, true);
    }
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" charset="UTF-8">
        <link rel="shortcut icon" href="../web/images/favicon.ico">
        <link rel="stylesheet" type="text/css" href="../web/css/css.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://informaticapc.com/base_js/lib.js"></script>
        <script src="https://informaticapc.com/boostrap/js/bootstrap.min.js"></script>
        <link href="https://informaticapc.com/boostrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $("#noticia").fadeOut(1500);
                }, 3000);

            });
        </script>
        <style>
            body {
                background:lightskyblue;
                width: 100%;
                font-family: Georgia, "Time New Roma", Times, serif;
            }
        </style>
    </head>
    <body>
        <form name="Aemet" action="index.php?ctl=mashupAemet" method="POST">
            <h2>Datos de AEMET.</h2>
            <div id="noticia">
                <h3>Último informe elaborado de 24 h.</h3>
            </div>
            <br>
            <a class="btn btn-primary pull-left" href="index.php?ctl=inicio">Volver</a>
            <fieldset>
                <legend style="text-align: center"><h2>Valencia-Barcelona-Madrid</h2></legend>
                <div class="form-group">
                    <select name="estaciones" id="estaciones">
                        <option value="estaciones-Valencia" selected>Valencia</option>
                        <option value="estaciones-Madrid">Madrid</option>
                        <option value="estaciones-Barcelona">Barcelona</option>
                        <option value="estaciones-Bizkaia">Bilbao</option>
                        <option value="estaciones-Sevilla">Sevilla</option>
                    </select>
                    <?php
                    if (isset($_POST['enviar'])) {
                        $_POST['estaciones-Valencia'];
                        $_POST['estaciones-Madrid'];
                        $_POST['estaciones-Barcelona'];
                        $_POST['estaciones-Sevilla'];
                        $_POST['estaciones-Bizkaia'];

                        $estaciones = $_POST['estaciones'];

                        switch ($estaciones) {
                            case 'estaciones-Valencia':
                                ?>
                                <select name="estaciones" id="estaciones">
                                    <option value="">----Seleccione una estación----</option>
                                    <option value="8381X">Ademuz</option>
                                    <option value="8072Y">Barx</option>
                                    <option value="8270X">Bicorp</option>
                                    <option value="8319X">Buñol</option>
                                    <option value="8300X">Carcaixent</option>
                                    <option value="8395X">Chelva</option>
                                    <option value="8290X">Enguera-Navalón</option>
                                    <option value="8193E">Jalance</option>
                                    <option value="8409X">Liria</option>
                                    <option value="8058Y">Miramar</option>
                                    <option value="8058X">Oliva</option>
                                    <option value="8283X">Ontinyent</option>
                                    <option value="8325X">Polinyà del Xúquer</option>
                                    <option value="8446Y">Sagunto</option>
                                    <option value="8337X">Turís</option>
                                    <option value="8309X">Utiel</option>
                                    <option value="8416Y">Valencia</option>
                                    <option value="8414A">Valencia-Aeropuerto</option>
                                    <option value="8293X">Xàtiva</option>
                                    <option value="8203O">Zarra</option>
                                </select>
                                <?php
                                break;
                            case 'estaciones-Madrid':
                                ?>
                                <select name="estaciones" id="estaciones">
                                    <option value="">----Seleccione una estación----</option>
                                    <option value="3170Y">Alcala de Henares</option>
                                    <option value="3268C">Alpedrete</option>
                                    <option value="3100B">Aranjuez</option>
                                    <option value="3182Y">Aranda del Rey</option>
                                    <option value="3110C">Buitrago del Lozoya</option>
                                    <option value="3191E">Colmenar Viejo</option>
                                    <option value="3129">Madrid Aeropuerto</option>
                                    <option value="3194U">Madrid - Ciudad Universitaria</option>
                                    <option value="3126Y">Madrid - El Goloso</option>
                                    <option value="3195">Madrid - Retiro</option>
                                    <option value="3194Y">Pozuelo de Alarcón</option>
                                    <option value="326A">Puerto Alto del León</option>
                                    <option value="2462">Puerto e Navacerrada</option>
                                    <option value="3104Y">Rascafría</option>
                                    <option value="3338">Robledo de Chavela</option>
                                    <option value="3330Y">Rozas de Puerto Real</option>
                                    <option value="3125Y">San Sebastián de los Reyes</option>
                                    <option value="3229Y">Tielmes</option>
                                    <option value="3343Y">Valdemorillo</option>
                                </select>
                                <?php
                                break;
                            case 'estaciones-Bizkaia':
                                ?>
                                <select name="estaciones" id="estaciones">
                                    <option value="">----Seleccione una estación----</option>
                                    <option value="1069Y">Abadiño</option>
                                    <option value="1074C">Amorebieta-Etxano</option>
                                    <option value="1078C">Balmaseda</option>
                                    <option value="1082">Bilbao Aeropuerto</option>
                                    <option value="1056K">Forua</option>
                                    <option value="1078I">Güeñes</option>
                                    <option value="1055B">Lekeitio</option>
                                    <option value="1057B">Machichaco</option>
                                    <option value="1046L">Orozko</option>
                                    <option value="1059X">Punta Galea</option>
                                    <option value="1083B">Sopuerta</option>
                                </select>
                                <?php
                                break;
                            case 'estaciones-Barcelona':
                                ?>
                                <select name="estaciones" id="estaciones">
                                    <option value="">----Seleccione una estación----</option>
                                    <option value="0252D">Arenys de Mar</option>
                                    <option value="0106X">Balsareny</option>
                                    <option value="0201D">Barcelona</option>
                                    <option value="0076">Barcelona Aeropuerto</option>
                                    <option value="0201X">Barcelona - Museo Maritimo</option>
                                    <option value="0092X">Berga</option>
                                    <option value="0222X">Caldes de Montbui</option>
                                    <option value="0194D">Corbera - Pic d' Agulles</option>
                                    <option value="0260X">Fogars de Montclús</option>
                                    <option value="0171X">Igualada</option>
                                    <option value="0149X">Manresa</option>
                                    <option value="0120X">Moiá</option>
                                    <option value="0158X">Monistrol de Montserrat</option>
                                    <option value="0061X">Pontons</option>
                                    <option value="0114X">Prats de Lluçanés</option>
                                    <option value="0255B">Santa Susanna</option>
                                    <option value="0073X">Sitges</option>
                                    <option value="0066X">Vilafranca del Penedés</option>
                                    <option value="0244X">Vilassar de Dalt</option>
                                </select>
                                <?php
                                break;
                            case 'estaciones-Sevilla':
                                ?>
                                <select name="estaciones" id="estaciones">
                                    <option value="">----Seleccione una estación----</option>
                                    <option value="5733X">Almadén de la Plata</option>
                                    <option value="5702X">Carmona</option>
                                    <option value="5835">Carrión de los Céspedes</option>
                                    <option value="5704B">Cazalla de la Sierra</option>
                                    <option value="5656">Fuentes de Andalucía</option>
                                    <option value="5726X">Guadalcanal</option>
                                    <option value="5654X">La Puebla de los Infantes</option>
                                    <option value="5612B">La Roda de Andalucia</option>
                                    <option value="5891X">La Cabezas de San Juan</option>
                                    <option value="5612X">Lora de Estepa</option>
                                    <option value="5796">Morón de la Frontera</option>
                                    <option value="5998X">Osuna</option>
                                    <option value="5783">Sevilla Aeropuerto</option>
                                    <option value="5790Y">Sevilla, Tablada</option>
                                    <option value="5788X">Tomarés, Zaudín</option>
                                    <option value="5641X">Écija</option>
                                </select>
                                <?php
                                break;
                        }
                    }
                    ?>

                    </select>
                    <button class="btn btn-primary pull-center" type="submit" name="enviar">Solicitar</button>
                </div>
            </fieldset>
        </form>
        <hr>
        <div id="mostrarDatos">
            <ul>
                <?php
                if (isset($_REQUEST["estaciones"])) { // si se ha realizado submit
// si no hay datos
                    if ($datos_fuenteDatos == null) { // imprimimos mensaje
                        echo "Datos no disponibles en este momento. Inténtelo más tarde.";
                    } else { // si hay datos obtenemos los registros de la estación metereológica guardamos los valores en variables
                        foreach ($datos_fuenteDatos as $value) {
                            $codigo_estacion = $value["idema"];
                            $nombre_estacion = $value["ubi"];
                            $longitud = $value["lon"];
                            $latitud = $value["lat"];
                            $altitud = $value["alt"];
                            $rango_hora = $value["fint"];
                            $prec_acumulada = (isset($value["prec"]) && $value["prec"] != "") ? $value["prec"] . "
l/m2" : "Sin predicción";
                            $prec_liq_acumulada = (isset($value["pliqtp"]) && $value["pliqtp"] != "") ?
                                    $value["pliqtp"] . " l/m2" : "Sin predicción";
                            $prec_sol_acumulada = (isset($value["psolt"]) && $value["psolt"]) ? $value["psolt"] . "
l/m2" : "Sin predicción";
                            $vel_max_viento = (isset($value["vmax"]) && $value["vmax"] != "") ? $value["vmax"] . "
m/s" : "Sin predicción";
                            $vel_md_viento = (isset($value["vv"]) && $value["vv"] != "") ? $value["vv"] . " m/s" :
                                    "Sin predicción";
                            $dir_md_viento = (isset($value["dv"]) && $value["dv"] != "") ? $value["dv"] . " m/s" :
                                    "Sin predicción";
                            $humedad_relativa = (isset($value["hr"]) && $value["hr"] != "") ? $value["hr"] . " %" :
                                    "Sin predicción";
                            $presion_atmosferica = (isset($value["pres"]) && $value["pres"] != "0") ? $value["pres"]
                                    . " hPr" : "Sin predicción";
                            $temperatura = (isset($value["ts"]) && $value["ts"] != "") ? $value["ts"] . " °" : "Sin predicción";
                            $temperatura_aire = (isset($value["ta"]) && $value["ta"] != "") ? $value["ta"] . " °" : "Sin predicción";
                            $nieve = (isset($value["nieve"]) && $value["nieve"] != "") ? $value["nieve"] . " cm" : "Sin predicción";
// generamos html para mostar los datos en el DOM
                            echo "
        <li>
            <h1 style='text-align: center'>$codigo_estacion - $nombre_estacion</h1>
            <h2 style='text-align: center'>Latitud: $latitud ° - Longitud: $longitud ° , Altitud: $altitud m</h2>
            <h3 style='text-align: center'>Tramo horario: $rango_hora</h3>
            <ul>
                <li>Temperatura Suelo: $temperatura</li>
                <li>Temperatura Aire: $temperatura_aire</li>
            </ul>
            <ul>
                <li>Velocidad Max. del Viento: $vel_max_viento</li>
                <li>Velocidad Med. del Viento: $vel_md_viento</li>
                <li>Dirección del Viento: $dir_md_viento </li>
            </ul>
            <ul>
                <li>Precipitación Acumulada: $prec_acumulada</li>
                <li>Precipitación Líquida Acumulada: $prec_liq_acumulada</li>
                <li>Precipitación Sólida Acumulada: $prec_sol_acumulada</li>
            </ul>
            <ul>
                <li>Humedad Relativa: $humedad_relativa</li>
            </ul>
            <ul>
                <li>Presión Atmosférica: $presion_atmosferica</li>
            </ul>
            <ul>
                <li>Espesor de nieve: $nieve</li>
            </ul>
        </li>
            ";
                        }
                    }
                }
                ?>
            </ul>
            <hr>
        </div>
    </body>
</html>


