
<?php 
    session_start(); 
    ob_start();
    
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    if ($_SESSION['SesionValida'] == 0) {
    header("Location:../web/index.php");
    }
   
    require_once __DIR__ .'../../../app/Mysql.php'; 
    require_once __DIR__ .'../../../app/Postgres.php';
    require_once __DIR__ . '../../../app/cliente.php';

    $cliente = new cliente();
    $result = $cliente->ListarCl();

    $json_string = json_encode($result);

    $file = '../../ficheros/clientes.json';
    $fp = fopen($file, 'w');
    fwrite($fp, $json_string);

    ob_clean();
    fclose($fp);

    header('Content-Type: application/json');
    readfile($file);
//header('Location: '.$file);
    ?>
    
