<?php
ob_start();
session_start();

error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" charset="UTF-8">
        <link rel="shortcut icon" href="../web/images/favicon.ico">
        <link rel="stylesheet" type="text/css" href="../web/css/css.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://informaticapc.com/base_js/lib.js"></script>
        <script src="https://informaticapc.com/boostrap/js/bootstrap.min.js"></script>
        <link href="https://informaticapc.com/boostrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
        <link rel="alternate" type="application/rss+xml" title="mi RSS" href="Cliente_rss.php" />
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $("#cuerpo").fadeOut(1500);
                }, 3000);
            });
        </script>
    </head>
    <body>
        <div id="cuerpo">
            <article>
               <?php
                if ($_SESSION['SesionValida'] == 0) {
                    header("Location:../../web/index.php");
                } else {
                    ?>
                    <br><font  style='color:darkmagenta'><h3>Bienveni@ a nuestra tienda.<br>
                         <?php echo '<h4>Conectado la base de datos:  </h4><h3><b>'. $_SESSION['datos'] .'</b></h3>';?>
                    <?php } ?></h3></font>

            </article>
        </div>
        <div>
            <h1 class="page-header">Tabla Clientes</h1>
        </div>
        <div class="breadcrumb">
            <a class="btn btn-info" href="index.php?ctl=bitbucket"><img style="height: 40px; width: 40px;margin-left: 5px;" src="../web/images/bitbucket.jpg"></a>&nbsp;&nbsp;
            <a class="btn btn-info" href="index.php?ctl=documentacion"><img style="height: 40px; width: 40px;margin-left: 5px;" src="../web/images/pdf.png"></a>
        </div>
        <div class="panel-heading">
            <a class="btn btn-primary pull-left" href="index.php?ctl=CrudCliente">Agregar</a>&nbsp;&nbsp;
            <a class="btn btn-primary pull-center" href="index.php?ctl=buscarCliente">Buscar</a>
            <a class="btn btn-warning pull-right" onclick="javascript:return confirm('¿Seguro que quiere salir?');" href="index.php?ctl=cerrarSesion"><img style="height: 15px; width: 15px;margin-left: 5px;" src="../web/images/logout.png">Cerrar</a>
        </div>

        <table class="table  table-striped  table-hover" id="tabla">
            <thead>
                <tr>
                    <th style="width:20px; background-color: #5DACCD; color:#fff" type="hidden">Id</th>
                    <th style="width:100px; background-color: #5DACCD; color:#fff">DNI</th>
                    <th style="width:150px; background-color: #5DACCD; color:#fff">Nombre</th>
                    <th style="width:150px; background-color: #5DACCD; color:#fff">Apellido</th>
                    <th style="width:180px; background-color: #5DACCD; color:#fff">Correo</th>
                    <th style="width:90px; background-color: #5DACCD; color:#fff">Telefono</th>
                    <th style="width:100px; background-color: #5DACCD; color:#fff">Usuario</th>   
                    <th style="width:60px; background-color: #5DACCD; color:#fff">Password</th>
                    <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
                    <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
                    <th style="width:60px; background-color: #5DACCD; color:#fff"></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    //$datos = $_SESSION['datos'];
                    switch ($_SESSION['datos']) {
                        case 'Mysql':
                            $db = new Mysql();
                            break;
                        case 'Postgres':
                            $db = new Postgres();
                            break;
                    }
                    $result = $db->ListarCl();
                    ?>
                <?php foreach ($result as $registro): ?>
                <tr>
                        <td><?php echo $registro->id; ?></td>
                        <td><?php echo $registro->dni; ?></td>
                        <td><?php echo $registro->Nombre; ?></td>
                        <td><?php echo $registro->Apellido; ?></td>
                        <td><?php echo $registro->Correo; ?></td>
                        <td><?php echo $registro->Telefono; ?></td>
                        <td><?php echo $registro->usuario; ?></td>
                        <td><?php echo $registro->password; ?></td>
                        <td>
                            <a class="btn btn-success" href="index.php?ctl=CrudCliente&id=<?php echo $registro->id; ?>">Actualizar</a>
                        </td>
                        <td>
                            <a class="btn btn-primary" href='index.php?ctl=verCliente&id=<?php echo $registro->id; ?>'>Ver</a>
                        </td>
                        <td>
                            <a class="btn btn-danger" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="index.php?ctl=borrarCliente&id=<?php echo $registro->id; ?>">Eliminar</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table> 
        <hr>
        <div class="breadcrumb">
            <a class="btn btn-secondary pull-left" href="index.php?ctl=jsonCliente"><img style="height: 40px; width: 40px;margin-left: 5px;" src="../web/images/json.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary pull-left" type="application/rss+xml" href="index.php?ctl=rssCliente"><img style="height: 40px; width: 40px;margin-left: 5px;" src="../web/images/rss.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary pull-left" href="index.php?ctl=csvCliente"><img style="height: 40px; width: 40px;margin-left: 5px;" src="../web/images/csv.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary pull-left" href="index.php?ctl=mashupTNT"><img style="height: 40px; width: 40px;margin-left: 5px;" src="../web/images/TheNewYorkTimes_Time.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary pull-left" href="index.php?ctl=mashupAemet"><img style="height: 40px; width: 40px;margin-left: 5px;" src="../web/images/aemet-ico.jpg"></a>&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <br>
    </body>
</html>
<script 
    src="assets/js/datatable.js">
</script>