<?php
session_start();
ob_start();

error_reporting(E_ALL);
ini_set('display_errors', '1');

if ($_SESSION['SesionValida'] == 0 )  {
    header("Location:../web/index.php");
}
$_SESSION['datos'] = $_POST['datos'];
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" charset="UTF-8">
        <link rel="shortcut icon" href="../web/images/favicon.ico">
        <link rel="stylesheet" type="text/css" href="../../web/css/css.css" />
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://informaticapc.com/base_js/lib.js"></script>
        <script src="https://informaticapc.com/boostrap/js/bootstrap.min.js"></script>
        <link href="https://informaticapc.com/boostrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
    </head>
    <body>
        <h1 class="page-header">
            <?php echo "<b>" . $cliente->id != null ? $cliente->Nombre . "</b>" : 'Actualizando Registro'; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="index.php?ctl=inicio"><b>Cliente</b></a></li>
            <li class="active"><?php echo $cliente->id != null ? $cliente->Nombre : '<h3>|  Registro</h3>'; ?></li>
        </ol>
        <table border="1" class="table  table-striped  table-hover" id="tabla" >
            <form id="frm-cliente" action="index.php?ctl=GuardarCliente" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend style="text-align: center"><h2>Datos cliente</h2></legend>
                    <input type="hidden" name="id" value="<?php echo $db->id; ?>" > 
                    <div class="form-group"><label>DNI</label><input type="text" maxlength="9" size="9" name="dni" value="<?php echo $cliente->dni; ?>" class="form-control" placeholder="Ingrese su dni" required></div>
                    <div class="form-group"><label>Nombre</label><input type="text"  size="50" name="Nombre" value="<?php echo $cliente->Nombre; ?>" class="form-control" placeholder="Ingrese su nombre" required></div>    
                    <div class="form-group"><label>Apellido</label><input type="text" size="50" name="Apellido" value="<?php echo $cliente->Apellido; ?>" class="form-control" placeholder="Ingrese su apellido" required></div>    
                    <div class="form-group"><label>Correo</label><input type="text" size="50" name="Correo" value="<?php echo $cliente->Correo; ?>" class="form-control" placeholder="Ingrese su correo electrónico" required></div>   
                    <div class="form-group"><label>Telefono</label><input type="text" size="50" name="Telefono" value="<?php echo $cliente->Telefono; ?>" class="form-control" placeholder="Ingrese su telefono" required></div>  
                    <div class="form-group"><label>Usuario</label><input type="text" size="50" name="usuario" value="<?php echo $cliente->usuario; ?>" class="form-control" placeholder="Ingrese su usuario" required></div>   
                    <div class="form-group"><label>Password</label><input type="password" size="50" name="password" value="<?php echo $cliente->password; ?>" class="form-control" placeholder="Ingrese su contraseña" required></div>   
                    <hr>
                    <div class="text-right">
                        <button class="btn btn-primary">Guardar</button>
                    </div>
                </fieldset>
            </form>
        </table>
        <div style='color: red'>


        </div>
    </body>
</html>

<script>
    $(document).ready(function () {
        $("#frm-cliente").submit(function () {
            return $(this).validate();
        });
    });
</script>
<script 
    src="../assets/js/datatable.js">
</script>

