<?php
session_start();
ob_start();

include_once '../app/Mysql.php';
include_once '../app/Postgres.php';

error_reporting(E_ALL);
ini_set('display_errors', '1');

if ($_SESSION['SesionValida'] == 0) {
    header("Location:../web/index.php");
}
switch ($_SESSION['datos']) {
    case 'Mysql':
        $db = new Mysql();
        break;
    case 'Postgres':
        $db = new Postgres();
        break;
}
?>

<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" charset="UTF-8">
        <link rel="shortcut icon" href="../web/images/favicon.ico">
        <link rel="stylesheet" type="text/css" href="../../web/css/css.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://informaticapc.com/base_js/lib.js"></script>
        <script src="https://informaticapc.com/boostrap/js/bootstrap.min.js"></script>
        <link href="https://informaticapc.com/boostrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
        <style>
            body {
                width: 100%;
                font-family: Georgia, "Time New Roma", Times, serif;
            }
        </style>
        <script>
            $(document).ready(function () {
                $("#frm-cliente").submit(function () {
                    return $(this).validate();
                });
            });

            src = "../../assets/js/datatable.js" >
        </script>
    </head>
    <body>
        <h1 class="page-header">
            <?php echo "<b>" . $cliente->id != null ? $cliente->Nombre . "</b>" : 'Ver Registro'; ?>
        </h1>
        <a class="btn btn-primary pull-left" href="index.php?ctl=inicio">Volver</a>

        <table border="1" class="table  table-striped  table-hover" id="tabla" >
            <fieldset>
                <legend style="text-align: center"><h2>Datos del Cliente</h2></legend>
                <div class="form-group">
                    <thead>
                        <tr>
                            <th style="width:5px; background-color: #5DACCD; color:#fff" type="hidden">Id</th>
                            <th style="width:30px; background-color: #5DACCD; color:#fff">DNI</th>
                            <th style="width:50px; background-color: #5DACCD; color:#fff">Nombre</th>
                            <th style="width:50px; background-color: #5DACCD; color:#fff">Apellido</th>
                            <th style="width:50px; background-color: #5DACCD; color:#fff">Correo</th>
                            <th style="width:30px; background-color: #5DACCD; color:#fff">Telefono</th>
                            <th style="width:10px; background-color: #5DACCD; color:#fff">Usuario</th>   
                            <th style="width:10px; background-color: #5DACCD; color:#fff">Password</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo $cliente->id; ?></td>
                            <td><?php echo $cliente->dni; ?></td>
                            <td><?php echo $cliente->Nombre; ?></td>
                            <td><?php echo $cliente->Apellido; ?></td>
                            <td><?php echo $cliente->Correo; ?></td>
                            <td><?php echo $cliente->Telefono; ?></td>
                            <td><?php echo $cliente->usuario; ?></td>
                            <td><?php echo $cliente->password; ?></td>
                        </tr>
                    </tbody>
                </div>
            </fieldset>
        </table>
        <br><br>
    </body>
</html>    



