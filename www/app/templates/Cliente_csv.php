<?php
    session_start(); 
    ob_start(); 

    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    if ($_SESSION['SesionValida'] == 0) {
        header("Location:../web/index.php");
    }
    require_once __DIR__ .'../../../app/Mysql.php'; 
    require_once __DIR__ .'../../../app/Postgres.php';
    require_once __DIR__ . '../../../app/cliente.php';

    $cliente = new cliente();
    $result = $cliente->ListarCl();

    $file = '../ficheros/clientes.csv';
    $fp = fopen($file, 'w');

    foreach ($result as $fields) {
        if (is_object($fields))
            $fields = (array) $fields;
        fputcsv($fp, $fields);
    }

    ob_clean();
    fclose($fp);
    header('Location: ' . $file);
?>
