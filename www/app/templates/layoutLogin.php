<?php
//session_start();
?>

<?php
/*
// Si han aceptado la política
if (isset($_REQUEST['politica-cookies'])) {
    // Calculamos la caducidad, en este caso un año
    $caducidad = time() + (60 * 2);
    // Crea una cookie con la caducidad
    setcookie('politica', '1', $caducidad);
}
*/
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="../web/css/css.css">
        <link rel="shortcut icon" href="../web/images/favicon.ico">
        <script type="text/javascript" src="../web/js/script.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://informaticapc.com/base_js/lib.js"></script>
        <script src="https://informaticapc.com/boostrap/js/bootstrap.min.js"></script>
        <link href="https://informaticapc.com/boostrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="styleSheet" href="https://informaticapc.com/base_css/estilos.css" />
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $("#cookies").fadeOut(1500);
                }, 3000);
            });
            // Inicialización de selección de fuente de datos
            $(document).ready(function () {
                $('.mdb-select').materialSelect();
                $('.select-wrapper.md-form.md-outline input.select-dropdown').bind('focus blur', function () {
                    $(this).closest('.select-outline').find('label').toggleClass('active');
                    $(this).closest('.select-outline').find('.caret').toggleClass('active');
                });
            });
  //mostar contraseña
            $(document).ready(function () {
                $('#mostrar').click(function () {
                    if ($(this).hasClass('fa-eye')) {
                        $('#password').removeAttr('type');
                        $('#mostrar').addClass('fa-eye-slash').removeClass('fa-eye');
                    } else{
                        //Establecemos el atributo y valor
                        $('#password').attr('type', 'password');
                        $('#mostrar').addClass('fa-eye').removeClass('fa-eye-slash');
                    }
                });

            });

        </script>
        <title>Login</title>
        <style>
            body {
                background:lightskyblue;
                width: 100%;
                font-family: Georgia, "Time New Roma", Times, serif;
            }
        </style>
    </head>
    <body>
        <div class="politica-Cookies">
            <?php if (!isset($_REQUEST['politica-cookies']) && !isset($_COOKIE['politica'])) { ?>
                <div id="loginForm-cookie">
                    <form action="index.php?ctl=login" method="POST" enctype="multipart/form-data">
                        <label><img style="height: 25px; width: 25px;align-items: center" src="../web/images/usuario.png">Usuario:<a style="color: red">*</a></label>
                        <input type="text" name="usuario" id="usuario" placeholder="Introduce el usuario" value=""><br>
                        <label><img style="height: 25px; width: 25px;align-items: center" src="../web/images/contrasena.png"> Contraseña:<a style="color: red">*</a></label>
                        <input type="password" name="password" id="password" placeholder="Introduce tu contraseña" value="12345"><br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 

                        <div class="row">
                            <div class="col-md-6 select-outline">
                                <select class="mdb-select md-form md-outline colorful-select dropdown-primary pull-left" name="datos">
                                    <option value="" disabled selected>Elija Base Datos</option>
                                    <option value="mysql">Mysql</option>
                                    <option value="postgres">Postgres</option>
                                </select>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a class="btn btn-primary pull-right" href="index.php?ctl=nuevoCliente"><img style="height: 20px; width: 20px;margin-left: 4px;" src="../web/images/web.png"></a><br>
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <br><br>
                        <!--Fuente de enviar-borrar -->
                        <input type="submit" name="enviar" class="btn" value="Enviar">
                        <input type="reset"  name="borrar" class="btn" value="Borrar">
                    </form>
                </div>
                <hr>
                <!-- Mensaje de cookies -->
                <h2>Cookies</h2>
                <span>¿Aceptas nuestras cookies?</span>
                <table border-collapse="separate" border-spacing="10px 5px">
                    <tr>
                        <td>
                            <p>Frank Riesco Mozo te informa sobre su Política de Privacidad respecto del tratamiento y protección de los datos de carácter personal de los usuarios y clientes que puedan ser recabados por la navegación o contratación de servicios a través del sitio Web CEEDCV.es.
                                En este sentido, el Titular garantiza el cumplimiento de la normativa vigente en materia de protección de datos personales, reflejada en la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y de Garantía de Derechos Digitales (LOPD GDD). Cumple también con el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas (RGPD).
                                El uso de sitio Web implica la aceptación de esta Política de Privacidad así como las condiciones incluidas en el Aviso Legal.</p>
                            <a style='color:blue' href='?politica-cookies=1' class="btn">Aceptar</a>
                            <a style='color:red'  href='https://www.aepd.es/es/politica-de-privacidad-y-aviso-legal' class="btn"><img style="height: 15px; width: 15px;color: red" src="../web/images/flecha-derecha.png"> &nbsp;Más info</a></a>
                        </td>
                    </tr>
                </table>
            <?php } else { ?>
                <div class="container">
                    <p><h3 style="text-align: center">Inicia sesión para acceder</h3></p><br>
                </div>
                <div id="loginForm">
                    <form action="../app/templates/comprobar_login.php" method="POST" enctype="multipart/form-data">
                        <legend style="text-align: center">DATOS DE ACCESO</legend>
                        <p><h6>Los campos marcados con <a style="color: red">*</a> son obligatorios.</h6></p>

                        <label><img style="height: 25px; width: 25px;align-items: center" src="../web/images/usuario.png">Usuario:<a style="color: red">*</a></label>
                        <input type="text" name="usuario" placeholder="Introduce el usuario" value="Frank"><br>
                        <label><img style="height: 25px; width: 25px;align-items: center" src="../web/images/contrasena.png"> Contraseña:<a style="color: red">*</a></label> 
                        <i class="fa fa-eye" id="mostrar"></i> 
                        <input id="password" type="password" name="password" class="form-control" placeholder="Introduce tu contraseña" value="12345">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
 <!--Fuente de datos-->
                        <div class="row">
                            <div class="col-md-6 select-outline">
                                <select name="datos" class="mdb-select md-form md-outline colorful-select dropdown-primary pull-left" >
                                    <option value="Mysql" selected >Mysql</option>
                                    <option value="Postgres">Postgres</option>
                                </select>
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a class="btn btn-primary pull-right" href="index.php?ctl=nuevoCliente"><img style="height: 20px; width: 20px;margin-left: 4px;" src="../web/images/web.png"></a><br>
                        </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <br><br>  
<!--Fuente de enviar-borrar-->
                        <input type="submit" name="enviar" class="btn" value="Enviar">
                        <input type="reset"  name="borrar" class="btn" value="Borrar">
                    </form>
                </div>
                <div id="cookies">
                    <hr>
                    <p style="text-align: center"><h3>Aceptadas las cookies!!</h3></p>
                </div>
            <?php } ?>
        </div>
    </body>
</html>
