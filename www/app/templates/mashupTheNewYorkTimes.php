<?php
    session_start();
    ob_start();
    
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    if ($_SESSION['SessionValida'] != 1) {
        header("Location:../web/index.php");
    }

//var_dump($resultado); 
//var_dump($url);
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" charset="UTF-8">
        <link rel="shortcut icon" href="../web/images/favicon.ico">
        <link rel="stylesheet" type="text/css" href="../../web/css/css.css" />
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://informaticapc.com/base_js/lib.js"></script>
        <script src="https://informaticapc.com/boostrap/js/bootstrap.min.js"></script>
        <link href="https://informaticapc.com/boostrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $("#ingles").fadeOut(1500);
                }, 3000);

            });
        </script>
        <style>
            body {
                background:lightskyblue;
                width: 100%;
                font-family: Georgia, "Time New Roma", Times, serif;
            }
        </style>
    </head>
    <body>
        <h1>Busca articulos en el New York Times</h1><br>
        <form name="formBusq" action="index.php?ctl=mashupTNT" method="POST" enctype="multipart/form-data">
            <table class="table  table-striped  table-hover" id="tabla">
                <a class="btn btn-primary pull-left" href="index.php?ctl=inicio">Volver</a>
                <fieldset>
                    <legend style="text-align: center"><h2 id="ingles">Texto a buscar (en inglés):</h2></legend>
                    <br>
                    <div class="form-group">
                        <tr>
                            <td><input type="text" size="20" name="texto" value="<?php $texto ?>" required></td>
                            <td>Busca desde:<input type="date" size="20" name="fecha_inicio" value="<?php $fecha_inicio ?>" required></td>
                            <td>Busca hasta:<input type="date" size="20" name="fecha_fin" value="<?php $fecha_fin ?>" required></td>
                            <td><a class="btn btn-primary pull-right"><input type="submit" name="buscar" value="Buscar"></a></td>
                        </tr>
                    </div>
                </fieldset>
            </table>
        </form>
    
        <?php if ($result != ''): ?>
        <table border="1" class="table  table-striped  table-hover" id="tabla">
            <?php foreach ($result->response->docs as $item) : ?>
                <table border="1" class="table  table-striped  table-hover" id="tabla">
                    <fieldset>
                        <legend>
                            <div class="form-group">
                                <tr>
                                    <td>
                                        <a href="<?php echo $item->web_url ?>"><?php echo $item->snippet ?></a><br><br>
                                    </td>
                                </tr>
                            </div>
                        </legend>
                    </fieldset>
                </table>
            <?php endforeach; ?> 
        </table>
<?php endif; ?>
        </body>
</html>
