<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', '1');

include_once('../../app/Mysql.php');
include_once('../../app/Postgres.php');
//include_once('../../app/templates/layoutLogin.php');

try {
    if (isset($_POST['usuario']) && isset($_POST['password']) && isset($_POST['datos'])) {
        //$datos = $_REQUEST['datos'];
        switch ($_POST['datos']) {
            case 'Mysql':
                $db = new Mysql();
                break;
            case 'Postgres':
                $db = new Postgres();
                break;
        }                   
            $usuario = htmlentities(addslashes($_POST['usuario']));
            $password = htmlentities(addslashes($_POST['password']));
            
            $numero_registro = $db->userPassword($usuario, $password);
            
            if ($numero_registro != 0) {
                $_SESSION['SesionValida'] = 1;
                $_SESSION['datos'] = $_POST['datos'];

                header("Location:../../web/index.php?ctl=inicio");
            } else {
                header("Location:../../web/index.php");
            }
    }
} catch (Exception $ex) {
    die("ERRROR: " . $ex->getMessage());
}
?> 
