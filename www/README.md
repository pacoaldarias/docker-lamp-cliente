#Examen Extraordinario

## Descripción
**Ejercicio - 4: 
Proyecto con Open Data: el alumno debe seleccionar fuentes de datos abiertos públicas y
defender un posible proyecto. Es obligatorio implementar el código de dicho proyecto e
indicar posible trabajo futuro. El posible trabajo futuro puede no estar hecho pero si se deben
de poder responder al profesor preguntas sobre implementación, como se haría, como se
organizaría, etc....

## Frameworks utilizados
* [Datatables](https://datatables.net/) - Tablas dinámicas de datos
* [Bootstrap](http://getbootstrap.com/) - Bootstrap Library for frontend design

## Authors
* **Frank Riesco Mozo** - *Alumno de DAW - CEEDCV* 
* **Paco Aldarias** - *Profesor de DWS - CEEDCV*