<?php

// web/index.php
error_reporting(E_ALL);
ini_set('display_errors', '1');

//Carga del modelo y los controladores
require_once __DIR__ . '../../app/Mysql.php';
require_once __DIR__ . '../../app/cliente.php';
require_once __DIR__ . '../../app/Postgres.php';
require_once __DIR__ . '../../app/Controller.php';
require_once __DIR__ . '../../app/ControllerDoc.php';
require_once __DIR__ . '../../app/ControllerMashup.php';
require_once __DIR__ . '../../app/ControllerClientes.php';
require_once __DIR__ . '../../app/ControllerRepositorio.php';

//Enrutamiento
$mapeo = array(
    'nuevoCliente' => array('controller' => 'Controller', 'action' => 'nuevoCliente'),
    'login' => array('controller' => 'Controller', 'action' => 'login'),
    'inicio' => array('controller' => 'Controller', 'action' => 'inicio'),
    'clientes' => array('controller' => 'Controller', 'action' => 'clientes'),
    'cerrarSesion' => array('controller' => 'ControllerClientes', 'action' => 'cerrarSesion'),
    'CrudCliente' => array('controller' => 'ControllerClientes', 'action' => 'CrudCliente'),
    'buscarCliente' => array('controller' => 'ControllerClientes', 'action' => 'buscarCliente'),
    'verCliente' => array('controller' => 'ControllerClientes', 'action' => 'verCliente'),
    'actualizarCliente' => array('controller' => 'ControllerClientes', 'action' => 'actualizarCliente'),
    'borrarCliente' => array('controller' => 'ControllerClientes', 'action' => 'borrarCliente'),
    'GuardarCliente' => array('controller' => 'ControllerClientes', 'action' => 'GuardarCliente'),
    'csvCliente' => array('controller' => 'ControllerClientes', 'action' => 'csvCliente'),
    'jsonCliente' => array('controller' => 'ControllerClientes', 'action' => 'jsonCliente'),
    'rssCliente' => array('controller' => 'ControllerClientes', 'action' => 'rssCliente'),
    'mashupTNT' => array('controller' => 'ControllerMashup', 'action' => 'mashupTNT'),
    'mashupAemet' => array('controller' => 'ControllerMashup', 'action' => 'mashupAemet'),
    'bitbucket' => array('controller' => 'ControllerRepositorio', 'action' => 'bitbucket'),
    'documentacion' => array('controller' => 'ControllerDoc', 'action' => 'documentacion'),
);

//Parseo análisis de la ruta
if (isset($_GET['ctl'])) {
    if (isset($mapeo[$_GET['ctl']])) {
        $ruta = $_GET['ctl'];
    } else {
        //Si la opción seleccionada no existe en el array de mapeo, mostramos pantalla de error
        header('Status: 404 Not Found');
        echo '<html><body><h1>Error 404: No existe la ruta <i>' .
        $_GET['ctl'] .
        '</p></body></html>';
        exit;
    }
} else {
    //Si no se ha seleccionado nada mostraremos pantalla del login
    $ruta = 'login';
}


//Cargamos el asociado a la acción seleccionada por el usuario 
// Ejecución del controlador asociado a la ruta
// Parseoo análisis de la ruta
$controlador = $mapeo[$ruta];

if(method_exists($controlador['controller'], $controlador['action'])) {
    call_user_func(array(new $controlador['controller'], $controlador['action']));
} else {
    header('Status: 404 Not Found');
    echo '<html><body><h1>Error 404: El controlador <i>' .
    $controlador['controller'] . '->' . $controlador['action'] .
    '</i> no existe</h1></body></html>';
}
?>

