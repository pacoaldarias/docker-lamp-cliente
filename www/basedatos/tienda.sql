/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Frank
 * Created: 16-may-2020
 */
CREATE TABLE cliente (
  id serial NOT NULL ,
  dni varchar(60)  NOT NULL,
  Nombre varchar(50) NOT NULL DEFAULT 0,
  Apellido varchar(50) NOT NULL DEFAULT 0,
  Correo varchar(50)  NOT NULL,
  Telefono varchar(60) NOT NULL,
  usuario varchar(45) DEFAULT NULL,
  password varchar(45) DEFAULT NULL
);

INSERT INTO cliente (id, dni, Nombre, Apellido, Correo, Telefono, usuario, password) VALUES
(1, '45567889J', 'Frank', 'Riesco', 'frankrm@yahoo.es', '655123456', 'frank', '12345'),
(2, '44898779Y', 'Paco', 'Aldarias', 'paco@yahoo.es', '655555555', 'paco', '12345'),
(3, '23878974T', 'Juan', 'De la Cruz', 'juancruz@gmail.com', '654578900', 'juan', '12345');

ALTER TABLE cliente
  ADD PRIMARY KEY (id);

COMMIT;
